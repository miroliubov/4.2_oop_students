package src.demo;

import src.models.Group;
import src.models.Student;
import src.service.IMarksCalculationService;
import src.service.MarksCalculationService;

public class DemoService implements IDemoService {

    @Override
    public void execute() {

        Student student1 = new Student("Mirolyubov V I", new int[]{5, 5, 5, 5, 5, 5, 5, 5, 5, 5});
        Student student2 = new Student("Petrov P P", new int[]{5, 4, 3, 2, 2, 5, 0, 0, 0, 0});
        Student student3 = new Student("Golubev A A", new int[]{4, 0, 5, 5, 3, 0, 4, 0, 4, 0});
        Group group1 = new Group("OAB 192-168-1-0", new Student[]{student1, student2, student3});

        Student student4 = new Student("Javin O P", new int[]{5, 5, 5, 5, 5, 5, 0, 0, 0, 0});
        Student student5 = new Student("Ivanon K L", new int[]{2, 4, 4, 0, 0, 0, 0, 0, 0, 0});
        Student student6 = new Student("Sizov E P", new int[]{0, 2, 0, 0, 5, 5, 0, 4, 5, 0});
        Group group2 = new Group("OAB 192-168-1-1", new Student[]{student4, student5, student6});

        Student student7 = new Student("Mosin J I", new int[]{0, 5, 5, 3, 5, 5, 0, 5, 4, 5});
        Student student8 = new Student("Antonov A A", new int[]{2, 2, 2, 2, 2, 2, 2, 2, 2, 2});
        Student student9 = new Student("Kalinin B K", new int[]{4, 0, 5, 5, 3, 4, 4, 4, 4, 5});
        Student student10 = new Student("Nikitin N N", new int[]{5, 0, 5, 5, 0, 0, 0, 0, 0, 5});
        Group group3 = new Group("OAB 192-168-1-2", new Student[]{student7, student8, student9, student10});

        IMarksCalculationService marksCalculationService = new MarksCalculationService();
        System.out.println("Средний балл группы " + group1.getGroupName() + ": " + marksCalculationService.countGroupAverageMarks(group1));
        System.out.println("Средний балл группы " + group2.getGroupName() + ": " + marksCalculationService.countGroupAverageMarks(group2));
        System.out.println("Средний балл группы " + group3.getGroupName() + ": " + marksCalculationService.countGroupAverageMarks(group3));

        System.out.println("Средний балл студента " + student5.getStudentName() + ": " + marksCalculationService.averageStudentMark(student5));
        System.out.println("Средний балл студента " + student3.getStudentName() + ": " + marksCalculationService.averageStudentMark(student3));

        System.out.println("Количество двоечников в группе " + group1.getGroupName() + ": " + marksCalculationService.numberOfLosers(group1));
        System.out.println("Количество двоечников в группе " + group2.getGroupName() + ": " + marksCalculationService.numberOfLosers(group2));
        System.out.println("Количество двоечников в группе " + group3.getGroupName() + ": " + marksCalculationService.numberOfLosers(group3));

        System.out.println("Количество отличников в группе " + group1.getGroupName() + ": " + marksCalculationService.numberOfFive(group1));
        System.out.println("Количество отличников в группе " + group2.getGroupName() + ": " + marksCalculationService.numberOfFive(group2));
        System.out.println("Количество отличников в группе " + group3.getGroupName() + ": " + marksCalculationService.numberOfFive(group3));

    }
}
