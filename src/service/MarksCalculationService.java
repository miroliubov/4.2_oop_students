package src.service;

import src.models.Group;
import src.models.Student;

public class MarksCalculationService implements IMarksCalculationService {

    @Override
    public double countGroupAverageMarks(Group group) {
        double averageMarksOfGroup = 0;
        int counter = 0;
        for (Student studentProgress : group.getStudents()) {
            averageMarksOfGroup += averageStudentMark(studentProgress);
            counter++;
        }
        return Math.round((averageMarksOfGroup / (counter * 1.0)) * 100.0)/ 100.0;
    }

    @Override
    public double averageStudentMark(Student student) {
        int[] studentMarks = student.getStudentProgress().getMarks();
        int sumOfMarks = 0;
        int counter = 0;
        for (int studentMark : studentMarks) {

            if (studentMark == 0) {
                continue;
            }

            sumOfMarks += studentMark;
            counter++;

        }
        return Math.round((sumOfMarks / (counter *1.0)) * 100.0)/ 100.0;
    }

    @Override
    public int numberOfLosers(Group group) {
        int counter = 0;
        for (Student studentMarks: group.getStudents()) {
            for (int marks: studentMarks.getStudentProgress().getMarks()) {
                if (marks == 2){
                    counter++;
                    break;
                }
            }
        }
        return counter;
    }

    @Override
    public int numberOfFive(Group group) {
        int counter = 0;
        for (Student studentMarks: group.getStudents()) {
            if (averageStudentMark(studentMarks) == 5.0){
                counter++;
            }
        }
        return counter;
    }
}
