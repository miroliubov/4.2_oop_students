package src.service;

import src.models.Group;
import src.models.Student;

public interface IMarksCalculationService {

    double countGroupAverageMarks(Group group);
    double averageStudentMark(Student student);
    int numberOfLosers(Group group);
    int numberOfFive(Group group);
}
