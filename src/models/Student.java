package src.models;

import src.service.MarksCalculationService;

public class Student {

    private String studentName;
    private StudentProgress studentProgress;
    private int averageMark;


    public Student(String studentName, int[] studentMarks) {
        this.studentName = studentName;
        studentProgress = new StudentProgress(studentMarks);
    }

    public StudentProgress getStudentProgress() {
        return studentProgress;
    }

    public String getStudentName() {
        return studentName;
    }
}
